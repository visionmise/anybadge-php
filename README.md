# anybadge-php
Version 1.0.0
|
A PHP Wrapper for [AnyBadge](https://pypi.org/project/anybadge/1.0.0/)

## Usage

Send HTTP GET traffic to script using PHP -S or Web Server

The URL will be structured using get parameters
```
./abp.php?label=MY LABEL&value=MY VALUE&color=ff0000&filename=MY FILE NAME
```

The `label` and `value` parameters are required.

The `color` and `filename` parameters are optional. When the `filename` parameter is omitted the filename will take the name of the `label` parameter.

---

## Requirements

### PHP 7.3+
```
apt install php
```

### Python PIP

```
apt install python-pip
```

### Anybadge

```
sudo su
cd ~
umask 022
pip install anybadge
```
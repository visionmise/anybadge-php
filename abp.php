<?php

    /**
     * Anybadge PHP
     * 
     * Wrapper for Anybadge
     * @link https://pypi.org/project/anybadge/
     * 
     * @author VisionMise <visionmise@psyn.app>
     * @category development badge creation
     * @version 1.0.0
     */



    /**
     * get HTTP Parameters
     * getHttpParam gets the GET parameters
     * from the HTTP request
     * 
     * Only accepts GET method
     * @return array GET Parameters
     */
    function getHttpParam() : array {

        //if GET is set, just return it
        if ($_GET and count($_GET) > 0) return $_GET;

        //if not, retrieve the query string
        $strParam   = $_SERVER['QUERY_STRING'] ?? '';

        //parse query string
        $getParam   = [];
        parse_str($strParam, $getParam);

        //return parsed string as array
        return $getParam;
    }



    /**
     * Badge Command
     * badgeCmd generates the needed anybadge command
     * string to execute for badge creation
     *
     * @param string      $label    Left-side badge title
     * @param string      $value    Right-side badge value
     * @param string|null $color    [optional] Right-sidge badge color hex value (without #)
     * @param string|null $filename [optional] Name of svg file
     * @return array Anybadge Command String and filename
     */
    function badgeCmd(string $label, string $value, ?string $color = null, ?string $filename = null) : array {

        //if the color is not set
        //set the default color hex value
        if (!$color) $color = '008800';

        //if the filename is not set
        //set the default filename
        //based on label
        if (!$filename) $filename = str_replace(" ", "_", strtolower($label));
        
        //create anybadge command
        //https://pypi.org/project/anybadge/
        $cmd    = "anybadge -o -f \"./{$filename}.svg\" --color \#{$color} -l \"${label}\" -v \"{$value}\"";

        //return the command string and filename
        return [
            'command'   => $cmd,
            'filename'  => "{$filename}.svg"
        ];
    }



    /**
     * Create Badge
     * createBadge executes anybadge using
     * the `exec` function.
     * 
     * Retrieves get parameters and validates
     * the input arguments. The shell command
     * is generated then executed
     *
     * @return integer exec result code
     */
    function createBadge() : ?string {

        //get arguments from GET
        $getParam   = getHttpParam();

        //validate parameters
        $label      = $getParam['label']    ?? 'Not Set';
        $value      = $getParam['value']    ?? '';
        $color      = $getParam['color']    ?? null;
        $filename   = $getParam['filename'] ?? null;

        //generate command string
        $cmd            = badgeCmd($label, $value, $color, $filename);
        $cmdString      = $cmd['command'];
        $cmdFilename    = $cmd['filename'];

        //execute anybadge
        $result         = -1;
        $stdOut         = [];
        exec($cmdString, $stdOut, $result);
        
        //return filename or null
        return ($result === 0) ? $cmdFilename : null;
    }



    /**
     * Undocumented function
     *
     * @param string|null $filename
     * @return void
     */
    function showBadgePage(?string $filename) {

        //if there is no filename
        //exit
        if (!$filename || !trim($filename)) return;

        //if the given filename does not exist
        //exit
        if (file_exists($filename) == false) return;

        //get the svg content
        //if there is no svg content then exit
        $svg            = file_get_contents($filename);
        if (!$svg)      return;

        //get the html preview template
        //if there is no html content then exit
        $template       = file_get_contents('template.html');
        if (!$template) return;

        //populate template values
        $find           = ['%title%', '%body%'];
        $replace        = [str_replace('.svg', null, $filename), $svg];
        $html           = str_replace($find, $replace, $template);

        //print html
        echo $html;
    }


    //generate new badge
    $newBadge   = createBadge();


    //show friendly badge page
    showBadgePage($newBadge);


    //done
    exit();

?>
